package com.mkpazon.propertylistings.error

import java.io.IOException

class WebRepositoryError(val code: Int, override val message: String?) : IOException()
