package com.mkpazon.propertylistings.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.mkpazon.propertylistings.BuildConfig
import com.mkpazon.propertylistings.data.RestAdapter
import com.mkpazon.propertylistings.data.WebRepository
import com.mkpazon.propertylistings.error.ErrorInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Singleton

// TODO update url
private const val BASE_URL = "http://demo0065087.mockable.io/test/"

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRestAdapter(retrofit: Retrofit) = RestAdapter(retrofit)

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, objectMapper: ObjectMapper): Retrofit {

        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val okhttpBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            okhttpBuilder.addNetworkInterceptor(StethoInterceptor())
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okhttpBuilder.addInterceptor(httpLoggingInterceptor)
        }
        okhttpBuilder.addInterceptor(ErrorInterceptor())
        return okhttpBuilder.build()
    }

    @Provides
    @Singleton
    fun provideObjectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .registerModule(KotlinModule())
                .propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
        return objectMapper
    }

    @Provides
    @Singleton
    fun provideWebRepository(restAdapter: RestAdapter) = WebRepository(restAdapter)
}