package com.mkpazon.propertylistings.di

import com.mkpazon.propertylistings.data.WebRepository
import com.mkpazon.propertylistings.ui.detail.DetailPresenter
import com.mkpazon.propertylistings.ui.home.HomePresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {

    @Provides
    @Singleton
    fun provideHomePresenter(dataSource: WebRepository) = HomePresenter(dataSource)

    @Provides
    @Singleton
    fun provideDetailPresenter() = DetailPresenter()

}