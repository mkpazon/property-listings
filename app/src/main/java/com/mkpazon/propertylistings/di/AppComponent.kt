package com.mkpazon.propertylistings.di

import com.mkpazon.propertylistings.ui.detail.DetailFragment
import com.mkpazon.propertylistings.ui.home.HomeFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, PresenterModule::class])
interface AppComponent {

    fun inject(target: HomeFragment)

    fun inject(target: DetailFragment)
}