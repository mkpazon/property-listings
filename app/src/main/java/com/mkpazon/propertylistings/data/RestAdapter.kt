package com.mkpazon.propertylistings.data

import com.mkpazon.propertylistings.model.data.PropertiesResponse
import io.reactivex.Flowable
import retrofit2.Retrofit
import retrofit2.http.GET
import javax.inject.Inject

class RestAdapter @Inject constructor(retrofit: Retrofit) {

    private val webService: WebService

    init {
        webService = retrofit.create(WebService::class.java)
    }

    interface WebService {
        @GET("properties")
        fun getPropertyListings(): Flowable<PropertiesResponse>
    }

    fun getPropertyListings(): Flowable<PropertiesResponse> {
        return webService.getPropertyListings()
    }
}