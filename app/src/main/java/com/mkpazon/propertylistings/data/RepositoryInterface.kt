package com.mkpazon.propertylistings.data

import com.mkpazon.propertylistings.model.view.PropertyViewModel
import io.reactivex.Flowable

interface RepositoryInterface {

    fun getPropertyListings() : Flowable<PropertyViewModel>

}