package com.mkpazon.propertylistings.data

import com.mkpazon.propertylistings.model.Owner
import com.mkpazon.propertylistings.model.Property
import com.mkpazon.propertylistings.model.view.PropertyViewModel
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class WebRepository @Inject constructor(var restAdapter: RestAdapter) : RepositoryInterface {

    override fun getPropertyListings(): Flowable<PropertyViewModel> {
        return restAdapter.getPropertyListings()
                .subscribeOn(Schedulers.io())
                .map { response ->
                    val properties = mutableListOf<Property>()
                    response.data.map {
                        val ownerDataModel = it.owner
                        val owner = Owner("${ownerDataModel.firstName} ${ownerDataModel.lastName}", ownerDataModel.avatar.medium.url)

                        val property = Property(
                                id = it.id,
                                description = it.description,
                                title = it.title,
                                address1 = it.location.address1,
                                address2 = it.location.address2,
                                photoUrl = it.photo.image.small.url,
                                owner = owner,
                                bedroomCount = it.bedrooms,
                                bathroomCount = it.bathrooms,
                                carSpotsCount = it.carspots
                        )
                        properties.add(property)
                    }
                    PropertyViewModel.success(properties)
                }

    }
}