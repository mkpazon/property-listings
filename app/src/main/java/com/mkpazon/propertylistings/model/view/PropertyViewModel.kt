package com.mkpazon.propertylistings.model.view

import com.mkpazon.propertylistings.model.Property

data class PropertyViewModel(val hasError: Boolean = false,
                             val isLoading: Boolean = false,
                             val errorMessage: String?,
                             var value: List<Property>?) {


    companion object {
        fun loading(): PropertyViewModel {
            return PropertyViewModel(false, true, null, null)
        }

        fun success(repoList: List<Property>): PropertyViewModel {
            return PropertyViewModel(false, false, null, repoList)
        }

        fun error(errorMessage: String? = null): PropertyViewModel {
            return PropertyViewModel(true, false, errorMessage, null)
        }
    }
}