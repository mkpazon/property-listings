package com.mkpazon.propertylistings.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Property(val id: Int,
                    val title: String = "",
                    val description: String = "",
                    val photoUrl: String = "",
                    val address1: String = "",
                    val address2: String = "",
                    val owner: Owner,
                    val bedroomCount: Int = 0,
                    val bathroomCount: Int = 0,
                    val carSpotsCount: Int = 0) : Parcelable