package com.mkpazon.propertylistings.model.data

data class AvatarDataModel(val medium: SizeDataModel)