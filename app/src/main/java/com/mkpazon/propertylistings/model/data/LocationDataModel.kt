package com.mkpazon.propertylistings.model.data

import com.fasterxml.jackson.annotation.JsonProperty

data class LocationDataModel(@JsonProperty("address_1") val address1: String,
                             @JsonProperty("address_2") val address2: String,
                             val suburb: String,
                             val postcode: String)
