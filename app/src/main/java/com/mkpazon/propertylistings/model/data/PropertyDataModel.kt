package com.mkpazon.propertylistings.model.data

data class PropertyDataModel(val title: String,
                             val id: Int,
                             val is_premium: Boolean,
                             val bedrooms: Int,
                             val bathrooms: Int,
                             val carspots: Int,
                             val description: String,
                             val price: Float,
                             val owner: OwnerDataModel,
                             val location: LocationDataModel,
                             val photo: PhotoDataModel)
