package com.mkpazon.propertylistings.model.data

data class PhotoUrlDataModel(val url: String)