package com.mkpazon.propertylistings.model.data

data class PhotoDataModel(val image: ImageDataModel)