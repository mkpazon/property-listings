package com.mkpazon.propertylistings.model.data

data class ImageDataModel(val small: PhotoUrlDataModel)