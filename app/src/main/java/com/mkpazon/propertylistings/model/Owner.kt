package com.mkpazon.propertylistings.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Owner(val name: String,
                 val photoUrl: String = "") : Parcelable