package com.mkpazon.propertylistings.model.data

data class PropertiesResponse(val data: List<PropertyDataModel>)
