package com.mkpazon.propertylistings.model.data

data class OwnerDataModel(val id: Int,
                          val firstName: String,
                          val lastName: String,
                          val avatar: AvatarDataModel)