package com.mkpazon.propertylistings.application

import android.support.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.mkpazon.propertylistings.di.AppComponent
import com.mkpazon.propertylistings.di.DaggerAppComponent
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import timber.log.Timber

class PropertyListingsApp : MultiDexApplication() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        initDagger()
        initTimber()
        initStetho()
        initUIL()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent
                .builder()
                .build()
    }

    private fun initTimber() {
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return String.format("[%s#%s:%s]", super.createStackElementTag(element), element.methodName, element.lineNumber)
            }
        })
    }

    private fun initStetho() {
        Stetho.initializeWithDefaults(this)
    }

    private fun initUIL() {
        val defaultDisplayOptions = DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build()

        val config = ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultDisplayOptions)
                .build()

        ImageLoader.getInstance().init(config)
    }
}