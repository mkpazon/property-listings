package com.mkpazon.propertylistings.ui.home

import com.mkpazon.propertylistings.model.Property

interface HomeView {
    fun updateList(value: List<Property>)
    fun showErrorMessage(errorMessage: String?)
    fun showLoadingIndicator()
    fun hideLoadingIndicator()
    fun showEmptyView(show: Boolean)
}