package com.mkpazon.propertylistings.ui.detail

import com.mkpazon.propertylistings.model.Property

class DetailPresenter {

    var detailView: DetailView? = null

    fun start(property: Property) {
        detailView?.updateId(property.id.toString())
        detailView?.updateTitle(property.title)
        detailView?.updateDescription(property.description)
    }
}