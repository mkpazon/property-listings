package com.mkpazon.propertylistings.ui.detail

interface DetailView {
    fun updateId(id: String)
    fun updateTitle(title: String)
    fun updateDescription(description: String)
}