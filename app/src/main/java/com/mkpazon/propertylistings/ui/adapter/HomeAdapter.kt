package com.mkpazon.propertylistings.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mkpazon.propertylistings.R
import com.mkpazon.propertylistings.model.Property
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.item_property.view.*

class HomeAdapter(private val items: MutableList<Property>, private val listener: OnPropertyClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mImageOptionsOwner: DisplayImageOptions = DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .showImageOnLoading(R.drawable.ic_default_profile24dp)
            .build()

    private val mImageOptionsProperty: DisplayImageOptions = DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .showImageOnLoading(R.drawable.ic_house_24dp)
            .build()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_property, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val property = items[position]
        val itemView = holder.itemView
        itemView.setOnClickListener { listener.onPropertyClick(property) }
        itemView.textViewTitle.text = property.title
        itemView.textViewAddress1.text = property.address1
        itemView.textViewAddress2.text = property.address2
        itemView.textViewOwner.text = property.owner.name
        itemView.textViewBedroomCount.text = property.bedroomCount.toString()
        itemView.textViewBathroomCount.text = property.bathroomCount.toString()
        itemView.textViewCarspotCount.text = property.carSpotsCount.toString()
        ImageLoader.getInstance().displayImage(property.photoUrl, itemView.imageViewProperty, mImageOptionsProperty)
        ImageLoader.getInstance().displayImage(property.owner.photoUrl, itemView.imageViewOwner, mImageOptionsOwner)
    }

    fun addAll(propertyList: List<Property>) {
        items.addAll(propertyList)
    }
}

class HomeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

interface OnPropertyClickListener {
    fun onPropertyClick(property: Property)
}