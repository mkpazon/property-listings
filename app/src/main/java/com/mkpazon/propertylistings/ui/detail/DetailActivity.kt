package com.mkpazon.propertylistings.ui.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mkpazon.propertylistings.R
import timber.log.Timber


private const val TAG_DETAIL_FRAGMENT = "fragment_detail"

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        intent.extras?.let {

            var fragment = supportFragmentManager.findFragmentByTag(TAG_DETAIL_FRAGMENT) as DetailFragment?
            if (fragment == null) {
                Timber.d("Fragment is null")
                fragment = DetailFragment.newInstance(it.getParcelable(DETAIL_FRAGMENT_ARG_PROPERTY))

                supportFragmentManager.beginTransaction()
                        .replace(R.id.frameDetailContainer, fragment, TAG_DETAIL_FRAGMENT)
                        .commit()
            }
        }
    }
}
