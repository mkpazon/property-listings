package com.mkpazon.propertylistings.ui.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.mkpazon.propertylistings.R
import com.mkpazon.propertylistings.model.Property
import com.mkpazon.propertylistings.ui.detail.DETAIL_FRAGMENT_ARG_PROPERTY
import com.mkpazon.propertylistings.ui.detail.DetailActivity
import com.mkpazon.propertylistings.ui.detail.DetailFragment
import kotlinx.android.synthetic.main.activity_home.*
import timber.log.Timber

private const val TAG_HOME_FRAGMENT = "fragment_home"
private const val TAG_DETAIL_FRAGMENT = "fragment_detail"

class HomeActivity : AppCompatActivity(), OnHomeFragmentInteractionListener {

    private var mIsDualPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        showHomeFragment()

        frameDetailContainer?.let {
            mIsDualPane = it.visibility == View.VISIBLE
        }
    }

    private fun showHomeFragment() {
        var fragment = supportFragmentManager.findFragmentByTag(TAG_HOME_FRAGMENT) as HomeFragment?
        if (fragment == null) {
            Timber.d("Fragment is null")
            fragment = HomeFragment.newInstance()

            supportFragmentManager.beginTransaction()
                    .replace(R.id.frameHomeContainer, fragment, TAG_HOME_FRAGMENT)
                    .commit()
        }
    }

    override fun onPropertySelected(property: Property) {
        Timber.d(".onPropertySelected")
        Timber.i("isDualPane:$mIsDualPane")
        textViewNoItemSelected?.visibility = View.GONE
        if (mIsDualPane) {
            val detailFragment = DetailFragment.newInstance(property)
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameDetailContainer, detailFragment, TAG_DETAIL_FRAGMENT)
                    .commit()
        } else {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(DETAIL_FRAGMENT_ARG_PROPERTY, property)
            startActivity(intent)
        }
    }
}
