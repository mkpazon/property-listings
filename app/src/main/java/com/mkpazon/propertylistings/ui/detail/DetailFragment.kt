package com.mkpazon.propertylistings.ui.detail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mkpazon.propertylistings.R
import com.mkpazon.propertylistings.application.PropertyListingsApp
import com.mkpazon.propertylistings.model.Property
import kotlinx.android.synthetic.main.fragment_detail.*
import timber.log.Timber
import javax.inject.Inject

const val DETAIL_FRAGMENT_ARG_PROPERTY = "propertyInfo"

class DetailFragment : Fragment(), DetailView {

    @Inject lateinit var mPresenter: DetailPresenter

    private var mProperty: Property? = null

    companion object {
        fun newInstance(property: Property) =
                DetailFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(DETAIL_FRAGMENT_ARG_PROPERTY, property)
                    }
                }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Timber.d(".onCreateView")
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mProperty = it.getParcelable(DETAIL_FRAGMENT_ARG_PROPERTY)
        }

        (activity?.application as PropertyListingsApp).appComponent.inject(this)
        mPresenter.detailView = this
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Timber.d(".onActivityCreated")
        super.onActivityCreated(savedInstanceState)
        mProperty?.let {
            mPresenter.start(it)
        }
    }

    override fun updateId(id: String) {
        textViewId.text = id
    }

    override fun updateTitle(title: String) {
        textViewTitle.text = title
    }

    override fun updateDescription(description: String) {
        textViewDescription.text = description
    }
}
