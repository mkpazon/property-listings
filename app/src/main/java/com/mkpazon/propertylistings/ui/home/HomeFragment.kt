package com.mkpazon.propertylistings.ui.home


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mkpazon.propertylistings.R
import com.mkpazon.propertylistings.application.PropertyListingsApp
import com.mkpazon.propertylistings.model.Property
import com.mkpazon.propertylistings.ui.adapter.HomeAdapter
import com.mkpazon.propertylistings.ui.adapter.OnPropertyClickListener
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import timber.log.Timber
import javax.inject.Inject

class HomeFragment : Fragment(), HomeView, OnPropertyClickListener {

    @Inject lateinit var mPresenter: HomePresenter

    private var mAdapter: HomeAdapter? = null
    private var mListener: OnHomeFragmentInteractionListener? = null

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Timber.d(".onCreateView")
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        // Initialize RecyclerView
        val recyclerView = view.recyclerViewProperties
        val layoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager

        view.swipeRefreshLayoutProperties.setOnRefreshListener { mPresenter.onSwipeRefresh() }

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as PropertyListingsApp).appComponent.inject(this)
        mPresenter.homeView = this
        retainInstance = true
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Timber.d(".onActivityCreated")
        super.onActivityCreated(savedInstanceState)
        if (mAdapter == null) {
            mPresenter.loadListings()
        } else {
            view?.recyclerViewProperties?.adapter = mAdapter
        }
    }

    override fun updateList(value: List<Property>) {
        Timber.d(".updateList")

        mAdapter = HomeAdapter(mutableListOf(), this)
        view?.recyclerViewProperties?.adapter = mAdapter
        mAdapter?.addAll(value)
        mAdapter?.notifyDataSetChanged()
    }

    override fun showErrorMessage(errorMessage: String?) {
        Timber.e(errorMessage)
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun showLoadingIndicator() {
        Timber.d(".showLoadingIndicator")
        swipeRefreshLayoutProperties.isRefreshing = true
    }

    override fun onDestroy() {
        mPresenter.stop()
        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnHomeFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnHomeFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onPropertyClick(property: Property) {
        mListener?.onPropertySelected(property)
    }

    override fun hideLoadingIndicator() {
        swipeRefreshLayoutProperties.isRefreshing = false
    }

    override fun showEmptyView(show: Boolean) {
        if(show) {
            constraintLayoutHome.visibility = View.GONE
            textViewEmptyList.visibility = View.VISIBLE
        } else {
            constraintLayoutHome.visibility = View.VISIBLE
            textViewEmptyList.visibility = View.GONE
        }
    }
}

interface OnHomeFragmentInteractionListener {
    fun onPropertySelected(property: Property)
}