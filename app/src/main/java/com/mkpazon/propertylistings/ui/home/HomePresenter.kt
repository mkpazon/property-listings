package com.mkpazon.propertylistings.ui.home

import com.mkpazon.propertylistings.data.RepositoryInterface
import com.mkpazon.propertylistings.model.view.PropertyViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.DisposableSubscriber
import timber.log.Timber
import javax.inject.Inject

class HomePresenter @Inject constructor(private val dataSource: RepositoryInterface) {

    private val mDisposables = CompositeDisposable()

    var homeView: HomeView? = null

    fun loadListings() {
        Timber.d(".loadListings")

        mDisposables.add(dataSource.getPropertyListings()
                .observeOn(AndroidSchedulers.mainThread())
                .startWith(PropertyViewModel.loading())
                .onErrorReturn { throwable -> PropertyViewModel.error(throwable.message) }
                .doOnTerminate { homeView?.hideLoadingIndicator() }
                .subscribeWith(object : DisposableSubscriber<PropertyViewModel>() {
                    override fun onNext(propertyViewModel: PropertyViewModel?) {
                        propertyViewModel?.let {
                            when {
                                it.hasError -> homeView?.showErrorMessage(it.errorMessage)
                                it.isLoading -> homeView?.showLoadingIndicator()
                                else -> it.value?.let {
                                    if(!it.isEmpty()) {
                                        homeView?.updateList(it)
                                    } else {
                                        homeView?.showEmptyView(true)
                                    }
                                }
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        Timber.d(e, ".onError")
                    }

                    override fun onComplete() {
                        Timber.d(".onComplete")
                    }
                })
        )
    }

    fun stop() {
        mDisposables.clear()
    }

    fun onSwipeRefresh() {
        loadListings()
        homeView?.hideLoadingIndicator()
    }
}