package com.mkpazon.propertylistings.ui.home

import com.mkpazon.propertylistings.data.RepositoryInterface
import com.mkpazon.propertylistings.model.Property
import com.mkpazon.propertylistings.model.view.PropertyViewModel
import com.mkpazon.propertylistings.ui.home.data.TEST_ERROR_MESSAGE
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableOnSubscribe
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomePresenterTest {

    companion object {
        @BeforeClass
        @JvmStatic
        fun setupClass() {
            // Override calls to AndroidSchedulers.mainThread() and Schedulers.io()
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
            RxJavaPlugins.initIoScheduler { Schedulers.trampoline() }
        }
    }

    @Mock
    lateinit var dataSource: RepositoryInterface

    @Mock
    lateinit var mView: HomeView

    private lateinit var presenter: HomePresenter

    @Before
    fun setup() {
        presenter = HomePresenter(dataSource)
        presenter.homeView = mView
    }

    @Test
    fun loadListings_pass() {
        val list: List<Property> = listOf()

        `when`(dataSource.getPropertyListings())
                .thenReturn(Flowable.just(PropertyViewModel.success(list)))

        presenter.loadListings()

        verify(mView, times(1)).showLoadingIndicator()
        verify(mView, times(1)).updateList(list)
    }

    @Test
    fun loadListings_loading() {
        `when`(dataSource.getPropertyListings())
                .thenReturn(Flowable.just(PropertyViewModel.loading()))

        presenter.loadListings()

        verify(mView, atLeastOnce()).showLoadingIndicator()
        verify(mView, never()).updateList(anyList())
    }

    @Test
    fun loadListings_fail() {
        `when`(dataSource.getPropertyListings())
                .thenReturn(Flowable.just(PropertyViewModel.error(TEST_ERROR_MESSAGE)))

        presenter.loadListings()

        verify(mView, atLeastOnce()).showLoadingIndicator()
        verify(mView, never()).updateList(anyList())
        verify(mView, times(1)).showErrorMessage(TEST_ERROR_MESSAGE)
    }

    @Test
    fun loadListings_fail_serverError() {

        // Simulate an exception being thrown within the flowable
        val flowable = Flowable.create(
                FlowableOnSubscribe<PropertyViewModel> {
                    throw RuntimeException(TEST_ERROR_MESSAGE)
                }, BackpressureStrategy.DROP)

        `when`(dataSource.getPropertyListings())
                .thenReturn(flowable)
        presenter.loadListings()

        verify(mView, atLeastOnce()).showLoadingIndicator()
        verify(mView, never()).updateList(anyList())
        verify(mView, times(1)).showErrorMessage(TEST_ERROR_MESSAGE)
    }
}