package com.mkpazon.propertylistings.ui.detail

import com.mkpazon.propertylistings.model.Owner
import com.mkpazon.propertylistings.model.Property
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailPresenterTest {

    @Mock
    lateinit var mView: DetailView

    private lateinit var presenter: DetailPresenter

    @Before
    fun setup() {
        presenter = DetailPresenter()
        presenter.detailView = mView
    }

    @Test
    fun testStart() {
        val property = Property(id = 123, title = "A funny looking house", description = "This is a placeholder describing how funny the house looks", owner = Owner(name = "Mark Pazon"))

        presenter.start(property)

        Mockito.verify(mView, Mockito.times(1)).updateId("123")
        Mockito.verify(mView, Mockito.times(1)).updateTitle("A funny looking house")
        Mockito.verify(mView, Mockito.times(1)).updateDescription("This is a placeholder describing how funny the house looks")
    }
}