# Property Listings App

This project makes use of Mode-View-Presenter architectural pattern. It currently has 2 activities:

* Home - displays a list of properties after retrieving it from a server
* Detail - shows a couple of information related to the property after clicking it. This activity is only used when

This project aims to demonstrate the following:

* MVP architectural pattern
* Support for orientation change
* Support for RTL
* Unit testing and mocking
* Dynamic Layout (when the device has enough space then it makes use of 2 fragments in a screen)
* Kotlin

## Getting Started

### Prerequisites

You will need the Android Studio 3.1.1 to be installed in your system

### Installing

Clone this repository

```
git clone git@bitbucket.org:mkpazon/property-listings.git
```

Run Android studio and select `Open` and navigate to the folder where the project has been cloned.


### Branches

All development has been done in the `develop` branch before it has been merged to the `master`. If you are intersted on the history of the project, you'll definitely find them in the develop branch.

Codebase for `develop` and `master` will be the same by the time this project is sent for your evaluation so it doesn't matter which branch you are in when building this app.

[A Successful Git Branching Model](http://nvie.com/posts/a-successful-git-branching-model/) - Efforts are done to follow the model in this post.

## Package Structure Overview

Here is a high level overview of how packages are structured

```
com.mkpazon.propertylistings
└───application
│   │ PropertyListingsApp - extends Application class
│
└───data - contains communication with entities related to data retrievel.
│          Contains the repository interface and web repository implementation.
│          If another repository is required, say a database repository, it will go to this
│          folder as well
└───di - dependency injection stuff (Dagger2)
└───error - contains error related classes.
└───model
│   └─── data - raw model classes after receiving data from a repository
│   └─── view - model classes related to the displaying of data to UI
└───ui - contains UI related classes. Contains the views and presenters
```

## Running the tests

The project contains unit tests that can be run straight from Android Studio

```
<root>
└───app
│   └───src
│       └───test
│           └───java
```
Right click the `java` folder and select `Run tests in Java`.

## Built With

* Android Studio
* Kotlin

### Libraries used

* Retrofit
* Okhttp3
* RxJava2
* Jackson
* Dagger 2
* Universal Image Loader
* [Circle Image View](https://github.com/hdodenhof/CircleImageView)
* Timber - for logging
* Stetho - for debugging
* Mockito - for tests


## Authors

* **Mark Kristopher Pazon** - [About Me](https://about.me/mkpazon)